﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : INewspaper
    {
        DataModelDataContext db = new DataModelDataContext();

        public bool DeletePost(int idN)
        {
            try
            {
                Post deletePost = (from post in db.Posts where post.ID == idN select post).FirstOrDefault();
                db.Posts.DeleteOnSubmit(deletePost);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Post> GetPost()
        {
            try
            {
                return (from post in db.Posts select post).ToList();
            }
            catch
            {
                return null;
            }
        }

        public bool AddPost(Post post)
        {
            var result = db.Categories.Where(x => x.ID == post.categoryID).FirstOrDefault();
            if (result == null)
            {
                return false;
            }
            else
            {
                db.Posts.InsertOnSubmit(post);
                db.SubmitChanges();
                return true;
            }

        }

        public bool UpdatePost(Post newPost)
        {
            try
            {
                Post UpdatePost = (from post in db.Posts where post.ID == newPost.ID select post).FirstOrDefault();
                UpdatePost.title = newPost.title;
                UpdatePost.content = newPost.content;
                UpdatePost.avatar = newPost.avatar;
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
